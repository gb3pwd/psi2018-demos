# Simple Monte Carlo simulation to showcase PyCUDA.
# A pedagogical, extremely simple code dealing with big numbers.
# 
# Originally written for a talk at PSI on 2018-06-07.
#
# More details at the URL
# https://www.manzari.org/talk/2018_psi/
#
# Luca
# manzari@kth.se

# non-GPU related
from __future__ import division
import numpy as np
import time
from math import ceil
import sys

# GPU related
import pycuda.autoinit
import pycuda.driver as drv
import pycuda.gpuarray
import pycuda.curandom

# this was a nice number of points
N = 100000000
# now we have a not-so-nice number of points
N = N * 100
print("Pi estimated with {} points.".format(N))

# how computationally expensive is for the memory?
# -- N pairs of float64 plus one vector to hold the results of the multiplication, hence the * 3
m = np.dtype(np.float64).itemsize * N * 3
# -- 1 GB = 10**9 Bytes
print("We have to store ~{:.1f} GB of data.".format(m/10**9))

# how much memory is available on the GPU?
(m_free, m_total) = drv.mem_get_info()
# -- 1 GB = 10**9 Bytes
# -- we use 90 % of the free memory, to be conservative
m_free = m_free * 0.9
print("Free on GPU: {:.1f} GB".format(m_free/10**9))

# how many iterations should we run?
iters = int(ceil(m/m_free))
print("We have to run the computation {} time(s) on GPU.".format(iters))

# should we go for it?
choice = raw_input("Run the computation? [Y/n] ")
if choice == 'n':
    sys.exit()

### GPU ###

# time start gpu
tsg = time.time()

# compute maximum length of the arrays we can use on gpu for our problem:
# 2 for holding the sets, a third for holding the results of the multiplication...
Ns = int(m_free / (3 * np.dtype(np.float64).itemsize))
# ...but don't use it unless needed
if N < Ns:
    Ns = N

# preallocating for the number of points inside the circle (0 as of now)
cg_alloc = np.asarray(0)
cg = pycuda.gpuarray.to_gpu(cg_alloc)

for iter in range(iters):

    # some visual feedback
    print("On iteration {} of {}.".format(iter+1, iters))

    # on the last iteration we may not need the whole memory
    if (N > Ns) and (iter == iters-1):
        Ns = int(N - Ns*(iters-1))

    # 1. generate N pairs of random numbers
    xg = pycuda.curandom.rand((Ns,));
    yg = pycuda.curandom.rand((Ns,));

    # 2. determine how many points lie inside the circlular segment
    dg = xg**2 + yg**2
    #    we use a little hack here since GPUs are not good at control tasks
    dg = dg - 1
    dg = pycuda.gpuarray.if_positive(dg, pycuda.gpuarray.zeros_like(xg), pycuda.gpuarray.ones_like(xg))
    cg = cg + pycuda.gpuarray.sum(dg)

# 3. get an estimate of pi
pg = cg/N*4

# 4. end time gpu
teg = time.time()

print("On GPU, Pi is {:>1.5f} and it takes {:>3.2f} seconds".format(float(pg.get()), teg-tsg))
