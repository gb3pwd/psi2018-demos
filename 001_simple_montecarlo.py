# Simple Monte Carlo simulation to showcase PyCUDA.
# A pedagogical, extremely simple code.
# 
# Originally written for a talk at PSI on 2018-06-07.
#
# More details at the URL
# https://www.manzari.org/talk/2018_psi/
#
# Luca
# manzari@kth.se

# non-GPU related
from __future__ import division
import numpy as np
import time

# GPU related
import pycuda.autoinit
import pycuda.driver as drv
import pycuda.gpuarray
import pycuda.curandom

# nice number of points
N = 100000000
print("Pi estimated with {} points.".format(N))

### CPU ###

# time start cpu
ts = time.time()

# 1. generate N pairs of random numbers
x = np.random.rand(N,);
y = np.random.rand(N,);

# 2. determine how many points lie inside the circlular segment
d = x**2 + y**2
c = len(d[d<1])

# 3. get an estimate of pi
p = c/N*4

# 4. end time cpu
te = time.time()
print("On CPU, Pi is {:>1.5f} and it takes {:>3.2f} seconds".format(p, te-ts))

### GPU ###

# time start gpu
tsg = time.time()

# 1. generate N pairs of random numbers
xg = pycuda.curandom.rand((N,));
yg = pycuda.curandom.rand((N,));

# 2. determine how many points lie inside the circlular segment
dg = xg**2 + yg**2
dg = dg - 1
#    we use a little hack here since GPUs are not good at control tasks
dg = pycuda.gpuarray.if_positive(dg, pycuda.gpuarray.zeros_like(xg), pycuda.gpuarray.ones_like(xg))
cg = pycuda.gpuarray.sum(dg)

# 3. get an estimate of pi
pg = cg/N*4

# 4. end time gpu
teg = time.time()

print("On GPU, Pi is {:>1.5f} and it takes {:>3.2f} seconds".format(float(pg.get()), teg-tsg))

### CONCLUSION ###
print("That's a {:.2f}x speedup!".format((te-ts)/(teg-tsg)))
