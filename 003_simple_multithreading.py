# Simple Monte Carlo simulation to showcase PyCUDA.
# A pedagogical, extremely simple code dealing with multiple GPUs.
#
# Look here for a more elegant way of handling multiple GPUs!
# https://wiki.tiker.net/PyCuda/Examples/MultipleThreads
#
# Originally written for a talk at PSI on 2018-06-07.
#
# More details at the URL
# https://www.manzari.org/talk/2018_psi/
#
# Luca
# manzari@kth.se

# Derived from a test case by Chris Heuser
# Also see FAQ about PyCUDA and threads.

# non-GPU related
from __future__ import division
import numpy as np
import time

# GPU related
import pycuda
# this time we don't use pycuda.autoinit
import pycuda.driver as drv
import pycuda.gpuarray
import pycuda.curandom

# a step ahead: threads!
import threading

# since there is no pycuda.autoinit, we have to manually initialize cuda
drv.init()

# we can poll the driver and ask for how many GPUs we have
ngpus = drv.Device.count()
print("We have {} device(s).".format(ngpus))

# Now let's solve our toy problem once more.

# nice number of points
N = 100000000
# we put our algorithm in a for loop just to make it last longer,
# such that we can take a peek at our gpus using nvidia-smi
iters = 5
print("Pi estimated with {} points.".format(N*10))

### Let's do something on GPU 0! ###

print("Working on GPU #0")

# we do what autoinit would do: initialize a computational context on a device
dev0 = drv.Device(0)
ctx0 = dev0.make_context()

# time start gpu
tsg = time.time()

# 0. preallocate on gpu
cg = pycuda.gpuarray.to_gpu(np.asarray(0))

for iter in range(iters):

    # some visual feedback
    print("GPU 0: iteration {:>2} of {:>2}.".format(iter+1, iters))
    
    # 1. generate N pairs of random numbers
    xg = pycuda.curandom.rand((N,));
    yg = pycuda.curandom.rand((N,));

    # 2. determine how many points lie inside the circlular segment
    dg = xg**2 + yg**2
    dg = dg - 1
    #    we use a little hack here since GPUs are not good at control tasks
    dg = pycuda.gpuarray.if_positive(dg, pycuda.gpuarray.zeros_like(xg), pycuda.gpuarray.ones_like(xg))
    cg = cg + pycuda.gpuarray.sum(dg)

# 3. get an estimate of pi
pg = cg/(N*iters)*4

# 4. end time gpu
teg = time.time()

print("On GPU #0, Pi is {:>1.5f} and it takes {:>3.2f} seconds".format(float(pg.get()), teg-tsg))

### Tidy up before leaving! ###

# we have to cleanly terminate our computational contexts
# pycuda.autoinit took care of this for us before
ctx0.pop()


### Let's do something on GPU 1 now! ###

print("Working on GPU #1")

# we just create another context on this device; this will be the active context
dev1 = drv.Device(1)
ctx1 = dev1.make_context()

# time start gpu
tsg = time.time()

# 0. preallocate on gpu
cg = pycuda.gpuarray.to_gpu(np.asarray(0))

for iter in range(iters):

    # some visual feedback
    print("GPU 1: iteration {:>2} of {:>2}.".format(iter+1, iters))
    
    # 1. generate N pairs of random numbers
    xg = pycuda.curandom.rand((N,));
    yg = pycuda.curandom.rand((N,));

    # 2. determine how many points lie inside the circlular segment
    dg = xg**2 + yg**2
    dg = dg - 1
    #    we use a little hack here since GPUs are not good at control tasks
    dg = pycuda.gpuarray.if_positive(dg, pycuda.gpuarray.zeros_like(xg), pycuda.gpuarray.ones_like(xg))
    cg = cg + pycuda.gpuarray.sum(dg)

# 3. get an estimate of pi
pg = cg/(N*iters)*4

# 4. end time gpu
teg = time.time()

print("On GPU #1, Pi is {:>1.5f} and it takes {:>3.2f} seconds".format(float(pg.get()), teg-tsg))

# tidy up before leaving
ctx1.pop()


### Now let's use everything we have at the same time! ###
print("Working on everything we have at our disposal!")

def a_single_thread(gpu_number):
    """our usual Monte Carlo toy problem"""

    #-1. activate a gpu context!
    dev = drv.Device(gpu_number)
    ctx = dev.make_context()
    
    # 0. preallocate on gpu
    cg = pycuda.gpuarray.to_gpu(np.asarray(0))

    for iter in range(iters):

        # some visual feedback
        print("GPU {}: iteration {:>2} of {:>2}.".format(gpu_number, iter+1, iters))
    
        # 1. generate N pairs of random numbers
        xg = pycuda.curandom.rand((N,));
        yg = pycuda.curandom.rand((N,));

        # 2. determine how many points lie inside the circlular segment
        dg = xg**2 + yg**2
        dg = dg - 1
        #    we use a little hack here since GPUs are not good at control tasks
        dg = pycuda.gpuarray.if_positive(dg, pycuda.gpuarray.zeros_like(xg), pycuda.gpuarray.ones_like(xg))
        cg = cg + pycuda.gpuarray.sum(dg)

    # 3. get an estimate of pi
    pg = cg/(N*iters)*4

    print("On GPU {}, Pi is {:>1.5f} and it takes {:>3.2f} seconds".format(gpu_number, float(pg.get()), teg-tsg))

    # tidy up before leaving
    ctx.pop()

    return

# we haven't started any thread yet
threads = []

# we start one thread per gpu
for gpu_number in range(ngpus):
    thr = threading.Thread(target=a_single_thread, args=(gpu_number,))
    threads.append(thr)
    thr.start()
